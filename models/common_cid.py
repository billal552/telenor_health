import datetime

#db = DAL('mysql://root:123@localhost/telenor_health')
db = DAL('postgres://postgres:postgres@localhost/telenor_health')

default_datetime=datetime.datetime.now()+datetime.timedelta(hours=0)
#http_password='mbh2016'

#-------------------------------------------------- Different database
#SQLite        sqlite://storage.db
#MySQL         mysql://username:password@localhost/test
#PostgreSQL    postgres://username:password@localhost/test
#MSSQL         mssql://username:password@localhost/test
#FireBird      firebird://username:password@localhost/test
#Oracle        oracle://username/password@test
#DB2           db2://username:password@test
#Ingres        ingres://username:password@localhost/test
#Informix      informix://username:password@test
#Google App Engine/SQL        google:sql
#Google App Engine/NoSQL      google:datastore

#-------------------------------------------------------------------
