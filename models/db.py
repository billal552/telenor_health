# -*- coding: utf-8 -*-
#########################################################################

response.generic_patterns = ['*'] if request.is_local else []

#########################################################################

from gluon.tools import Mail, Auth, Crud, Service, PluginManager, prettydate
mail = Mail()                                  # mailer
auth = Auth(db)                                # authentication/authorization
crud = Crud(db)                                # for CRUD helpers using auth
service = Service()                            # for json, xml, jsonrpc, xmlrpc, amfrpc
plugins = PluginManager()                      # for configuring plugins

mail.settings.server = 'logging' or 'smtp.gmail.com:587'  # your SMTP server
mail.settings.sender = 'you@gmail.com'         # your email
mail.settings.login = 'username:password'      # your credentials or None

auth.settings.hmac_key = 'sha512:e47731ed-be4d-4466-8ed6-bdd7c793babf'   # before define_tables()
#auth.define_tables()                           # creates all needed tables
auth.settings.mailer = mail                    # for user email verification
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.messages.verify_email = 'Click on the link http://'+request.env.http_host+URL('default','user',args=['verify_email'])+'/%(key)s to verify your email'
auth.settings.reset_password_requires_verification = True
auth.messages.reset_password = 'Click on the link http://'+request.env.http_host+URL('default','user',args=['reset_password'])+'/%(key)s to reset your password'

#########################################################################

crud.settings.auth = None        # =auth to enforce authorization on crud

#########################################################################

#-------------------------- global variable
import datetime
import string
from gluon.tools import fetch
import os
import copy
from operator import itemgetter

currentDateTime=str(default_datetime)[0:19] #2012-10-03 11:20:10
currentDate=str(default_datetime)[0:10]     #2012-10-03


#----------------------- signature table common for all
signature = db.Table(db, 'table_signature',    
    Field('created_by','string', default=session.uid),
    Field('created_on', 'datetime', default=default_datetime),
    Field('updated_by','string', update=session.uid),
    Field('updated_on', 'datetime', update=default_datetime)    
    )

#===========================================>

#----------
db.define_table('key_values',
                Field('skey','string',requires=[IS_NOT_EMPTY(),IS_LENGTH(50,error_message='enter maximum 50 character')]),
                Field('svalue','string',default=''),
                signature,
                migrate=True
                )

#----------
db.define_table('ttl_time',
                Field('next_execute_on', 'datetime', default=default_datetime),
                migrate=True
                )



