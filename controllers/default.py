# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

import ast

#---------------------------------- login page
def index():
    response.title='Login'
    return dict()
    
#---------------------------------- logout
def log_out():
    session.clear()    
    if (session.user_id==None):
        redirect(URL('index'))
    else:
        session.flash="Error in logout!"
        redirect(URL('home'))
        
    return dict()

#---------------------------------- main home page
def home():        
    redirect(URL('default','index'))
    
#------------------------------- get key values
def get_keys():   
    keyValuesRows = db().select(db.key_values.skey,db.key_values.svalue, orderby = db.key_values.skey)
    dict_data = {}
    if not keyValuesRows:
        return 'Keys not available!'
    else:
        for record in keyValuesRows:
            skey = record.skey
            svalue = record.svalue            
            dict_data[skey] = svalue
    
    #Reset TTL
    ttlRows = db().select(db.ttl_time.id,db.ttl_time.next_execute_on, limitby=(0, 1))    
    ttl_execute_on1=datetime.datetime.now() - datetime.timedelta(minutes=5)    
    if ttlRows:
        ttlRows[0].update_record(next_execute_on = ttl_execute_on1)
          
    return str(dict_data)

#------------------------------- get values with key
def get_values():   
    akeys=str(request.vars.akeys).strip().upper()
        
    try:
        akeys_list=akeys.split(',')
    except:
        return 'Request Error: 400'
    
        
    return_data = {}
    keyValuesRows = db((db.key_values.skey.belongs(akeys_list))).select(db.key_values.skey,db.key_values.svalue, orderby = db.key_values.skey)
    if not keyValuesRows:
        return 'Keys not available!'
    else:
        for record in keyValuesRows:
            skey = record.skey
            svalue = record.svalue            
            return_data[skey] = svalue
    
    #Reset TTL
    ttlRows = db().select(db.ttl_time.id,db.ttl_time.next_execute_on, limitby=(0, 1))    
    ttl_execute_on1=datetime.datetime.now() - datetime.timedelta(minutes=5)    
    if ttlRows:
        ttlRows[0].update_record(next_execute_on = ttl_execute_on1)
        
    return str(return_data)

#------------------------------- post values by key
def post_values():   
    akeys_dict_str=str(request.vars.akeys).strip().upper()
    
    try:
        akeys_dict = ast.literal_eval(akeys_dict_str)
    except:
        return 'Request Error: 400'
        
    for dict_key,dict_value in akeys_dict.iteritems():
        #keyValuesRows = db((db.key_values.skey == dict_key)).select(limitby=(0, 1))
        #if not keyValuesRows:        
        db.key_values.insert(skey=dict_key,svalue=dict_value)
        #else:
            #keyValuesRows[0].update_record(svalue=dict_value)
            
    return_data=str(akeys_dict)
    
    return return_data
    
#------------------------------- update values by key
def patch_values():   
    akeys_dict_str=str(request.vars.akeys).strip().upper()
    
    try:
        akeys_dict = ast.literal_eval(akeys_dict_str)
    except:
        return 'Request Error: 400'
        
    for dict_key,dict_value in akeys_dict.iteritems():
        keyValuesRows = db((db.key_values.skey == dict_key)).select(limitby=(0, 1))
        if keyValuesRows:        
            keyValuesRows[0].update_record(svalue=dict_value)
        
    return_data=str(akeys_dict)
    
    return return_data


def reset_data():    
    ttlRows = db().select(db.ttl_time.id,db.ttl_time.next_execute_on, limitby=(0, 1))
    
    ttl_execute_on1=datetime.datetime.now() - datetime.timedelta(minutes=5)
    
    if ttlRows:
        next_execute_on = ttlRows[0].next_execute_on
        
        db(db.key_values.created_on <= next_execute_on).delete()
        
        next_execute_on2 = next_execute_on + datetime.timedelta(minutes=1)
        ttlRows[0].update_record(next_execute_on = next_execute_on2)
        
    else:
                
        db.ttl_time.insert(next_execute_on = ttl_execute_on1)
    
    db.commit()
    
    return "ok"

