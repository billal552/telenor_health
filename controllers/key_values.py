
#---------------------------- 
def key_value_add():    
    response.title = 'Key Values'
    
    #   ---------------------
    form = SQLFORM(db.key_values,
                  fields=['skey', 'svalue'],
                  submit_button='Save'
                  )

    if form.accepts(request.vars, session):
       response.flash = 'Save Successfully'

    #  ---------------
    records = db().select(db.key_values.ALL, orderby=~db.key_values.created_on)
    
    return dict(form=form, records=records)

    
#---------------------------- EDIT
def key_value_edit():
    response.title = 'Key Values -Edit'
    
    rowID = request.args(0)
    
    record = db.key_values(rowID) or redirect(URL('key_value_add'))

    form = SQLFORM(db.key_values,
                  record=record,
                  deletable=True,
                  fields=['skey', 'svalue'],
                  submit_button='Update'
                  )

    if form.accepts(request.vars, session):
        response.flash = 'Updated Successfully'
        redirect(URL('key_value_add'))
        
    return dict(form=form,rowID=rowID)


